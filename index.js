const got = require('./data.js');

function countAllPeople(got) {
  // your code goes here
  const totalPeople = got.houses.reduce((countOfPeople, houseData) => {
    countOfPeople += houseData.people.length;
    return countOfPeople;
  }, 0);
  return totalPeople;
}

function peopleByHouses(got) {
  // your code goes here
  const houseData = got.houses.reduce((houseWithPeople, house) => {
    houseWithPeople[house.name] = house.people.length;
    return houseWithPeople;
  }, {});
  const arrayOfHouse = Object.entries(houseData)
  arrayOfHouse.sort();
  const objecthouse = Object.fromEntries(arrayOfHouse);
  return objecthouse;
}

function everyone(got) {
  // your code goes here
  const peopleArray = got.houses.map(peopelName => peopelName.people);
  const peopelName = peopleArray.reduce((names, peopelData) => {
    peopelData.forEach(nameInEach => names.push(nameInEach.name));
    return names;
  }, []);
  return (peopelName);
}

function nameWithS(got) {
  // your code goes here
  const peopelData = got.houses.map(peopleArray => peopleArray.people);
  const namesHaveS = peopelData.reduce((arrayOfs, peopleArray) => {
    peopleArray.map((nameOfS) => {
      if ((nameOfS.name).includes('s') || (nameOfS.name).includes('S')) {
        arrayOfs.push(nameOfS.name);
      }
    });
    return arrayOfs;
  }, []);
  return (namesHaveS);
}

function nameWithA(got) {
  // your code goes here
  const peopelData = got.houses.map(peopleArray => peopleArray.people);
  const peopleWithA = peopelData.reduce((nameOfA, peopleArray) => {
    peopleArray.forEach(namesHaveA => {
      if (namesHaveA.name.includes('a') || namesHaveA.name.includes('A')) {
        nameOfA.push(namesHaveA.name);
      }
    })
    return nameOfA;
  }, []);
  //console.log(peopleWithA);
  return peopleWithA;
}

function surnameWithS(got) {
  // your code goes here
  const peopleArray = got.houses.map(houseData => houseData.people);
  const lastNameS = peopleArray.reduce((lastWithS, arrayOfPeople) => {
    arrayOfPeople.forEach(nameLast => {
      let check = nameLast.name.split(' ');
      if (check[check.length - 1].includes('S')) {
        lastWithS.push(nameLast.name);
      }
    });
    return lastWithS;
  }, []);
  // console.log(lastNameS);
  return lastNameS;
}

function surnameWithA(got) {
  // your code goes here
  const peopleArray = got.houses.map(houseData => houseData.people);
  const lastNameA = peopleArray.reduce((lastWithA, arrayOfPeople) => {
    arrayOfPeople.forEach(nameLast => {
      let check = nameLast.name.split(' ');
      if (check[check.length - 1].includes('A')) {
        lastWithA.push(nameLast.name);
      }
    });
    return lastWithA;
  }, []);
  // console.log(lastNameS);
  return lastNameA;
}

function peopleNameOfAllHouses(got) {
  // your code goes here
  const houseData = got.houses.reduce((houseNames, currentHouse) => {
    const arrayOfNames = currentHouse.people.reduce((peopleArray, peopleObjects) => {
      //peopleArray(peopleObjects.name);
      let name = (peopleObjects.name);
      peopleArray.push(name);
      return peopleArray;
    }, []);
    houseNames[currentHouse.name] = arrayOfNames;
    return houseNames;
  }, {});
  const sortedHouse = Object.entries(houseData).sort();
  const finalResult = Object.fromEntries(sortedHouse);
  return finalResult;
}

// Testing your result after writing your function
console.log(countAllPeople(got));
// Output should be 33

console.log(peopleByHouses(got));
// Output should be
//{Arryns: 1, Baratheons: 6, Dothrakis: 1, Freys: 1, Greyjoys: 3, Lannisters: 4,Redwyne: 1,Starks: 8,Targaryens: 2,Tullys: 4,Tyrells: 2}

console.log(everyone(got));
// Output should be
//["Eddard "Ned" Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon "Bran" Stark", "Rickon Stark", "Jon Snow", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Queen Cersei (Lannister) Baratheon", "King Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Balon Greyjoy", "Theon Greyjoy", "Yara Greyjoy", "Margaery (Tyrell) Baratheon", "Loras Tyrell", "Catelyn (Tully) Stark", "Lysa (Tully) Arryn", "Edmure Tully", "Brynden Tully", "Olenna (Redwyne) Tyrell", "Walder Frey", "Jon Arryn", "Khal Drogo"]

console.log(nameWithS(got), 'with s');
// Output should be
// ["Eddard "Ned" Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon "Bran" Stark", "Rickon Stark", "Jon Snow", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Queen Cersei (Lannister) Baratheon", "Stannis Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Loras Tyrell", "Catelyn (Tully) Stark", "Lysa (Tully) Arryn"]

console.log(nameWithA(got));
// Output should be
// ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Cersei Baratheon", "Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Balon Greyjoy", "Yara Greyjoy", "Margaery Baratheon", "Loras Tyrell", "Catelyn Stark", "Lysa Arryn", "Olenna Tyrell", "Walder Frey", "Jon Arryn", "Khal Drogo"]

console.log(surnameWithS(got), 'surname with s');
// Output should be
// ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Jon Snow", "Catelyn Stark"]

console.log(surnameWithA(got));
// Output should be
// ["Lysa Arryn", "Jon Arryn"]

console.log(peopleNameOfAllHouses(got));
// Output should be
// {Arryns: ["Jon Arryn"], Baratheons: ["Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon"], Dothrakis: ["Khal Drogo"], Freys: ["Walder Frey"], Greyjoys: ["Balon Greyjoy", "Theon Greyjoy", "Yara Greyjoy"], Lannisters: ["Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Cersei Baratheon"], Redwyne: ["Olenna Tyrell"], Starks: ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Jon Snow"], Targaryens: ["Daenerys Targaryen", "Viserys Targaryen"], Tullys: ["Catelyn Stark", "Lysa Arryn", "Edmure Tully", "Brynden Tully"], Tyrells: ["Margaery Baratheon", "Loras Tyrell"]}